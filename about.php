<?php
session_start();
include 'functions.php';
?>
<?php
$link = mysqli_connect("localhost","root","","database") or die("Error " . mysqli_error($link));
?>
<!DOCTYPE html>
<html lang="en">
<title>Over Ons</title>
<?php include 'head.php' ?>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include 'nav.php' ?><!-- Vacature en events Section --> <!-- Vacature en events Section --> <!-- Vacature en events Section --> <!-- Vacature en events Section --> <!-- Vacature en events Section --> <!-- Vacature en events Section -->
<section>
    <div class="container" id="about-pic">
        <div class="row">


    <?php
    $query = "SELECT * FROM  markup WHERE id = 1";
    $result = $link->query($query);
    if($result->num_rows>0){
    while($row = $result->fetch_array()){
    ?>
    <div class="row col-lg-12 vacature">
        <div class="col-md-6 col-sm-6 portfolio-item">

            <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                    <i class="fa fa-plus fa-3x"></i>
                </div>
            </div>
            <img src="<?php echo $row['aboutimg']; ?>" width="400px"  height="352px" class="" alt="">
        </div>
        <div class="col-md-6 col-sm-6 portfolio-item">
            <div class="portfolio-caption">
                <h3> Over ons   </h3>
                <p class="text-muted"><?php echo $row['about']; ?></p>
            </div>
        </div>
    </div>
        </div>
    </div>
</section>
<?php
}
}
?>
<div id="hoe-het-werkt-about">
<?php include 'hoehetwerkt.php' ?>
</div>
<?php include 'footer.php'  ?>
<!-- jQuery -->

<style>
    .commentimg {
        width: 95px;
        height: 75px;
    }
    @media (min-width: 768px) {
        section {
            padding: 55px 0;
        }
    }

    .comments-users {
        text-align: left;
        padding: 40px;
    }
    .btn-xl {
        margin-bottom: 60px !important;
    }
    #contact{
        padding-bottom: 0px;

    }

    @media (max-width: 765px) {

        #hoe-het-werkt-about{

            margin-top: 300px;
        }
    }
    @media (min-width: 765px) {
        .text-muted {
            margin-left: 0px !important;
        }
        #hoe-het-werkt-about{

            margin-top: 50px;
        }
    }
    @media (max-width: 1000px)
        .portfolio-caption {
            margin-left: 10px !important;
        }

        #about-pic{

            margin-top:50px;

        }
        .portfolio-caption {
            margin-left: 0px ;
        }

    }
</style>
<?php include 'scripts.php' ?>
<?php include 'contact.php'  ?>
</body>

</html>
