<?php
session_start();
include 'functions.php';
?>
<?php
$link = mysqli_connect("localhost","root","","database") or die("Error " . mysqli_error($link));
?>
<!DOCTYPE html>
<html lang="en">
    <title>Events Overzicht</title>
<?php include 'head.php' ?>
<body id="page-top" class="index">
<!-- Navigation -->
<?php include 'nav.php' ?>
    <?php
    $query = "SELECT * FROM  events ";
    $result = $link->query($query);
    if($result->num_rows>0){
    while($row = $result->fetch_array()){
    ?>
        <section id="" class="">
            <div class="container">
                <div class="row">

        <div class="col-lg-6 col-sm-6 portfolio-item">

            <a href="events.php?event=<?php echo $row['id']; ?>" >
            <img src="<?php echo $row['img']; ?>" width="400px"  height="352px" class="" alt="">
                </a>
        </div>

        <div class="col-md-6 col-sm-6 portfolio-item">
            <div class="portfolio-caption">
                <h4 style="text-align: left;"> <?php echo $row['naam']; ?> | <?php echo $row['datum']; ?></h4>

                <p class="text-muted"><?php echo $row['tekst']; ?></p>
            </div>
        </div>

    </div>
    </div>
</section>
 </body>
    <style>
.text-muted {
    color: #777;
    margin-left: -298px;
}
section {
    margin-top: 150px;
}
</style>
<?php
    }
}
?>
<?php include 'hoehetwerkt.php' ?>

<?php include 'scripts.php' ?>
<?php include 'contact.php'  ?>