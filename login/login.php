<?php include 'core/int.php'; ?>
<?php include '../head.php' ?>
<?php
error_reporting(0);
session_start();
// errors 

    $error = '<div class="alert alert-warning alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <strong>Let op!</strong> gebruikersnaam of wachtwoord incorrent.
                  </div>';


if (isset($_POST['gebruikersnaam']) and isset($_POST['wachtwoord'])) {
    $gebruikersnaam = $_POST['gebruikersnaam'];
    $wachtwoord = md5($_POST['wachtwoord']);

   
    $result = mysqli_query($con,"SELECT * FROM gebruikers WHERE gebruikersnaam = '$gebruikersnaam'
    and wachtwoord = '$wachtwoord'") or die;
    $teller = mysqli_num_rows($result);
	
	$arr = mysqli_fetch_assoc($result);
	
	
	
    
    if ($teller == 1){
		$_SESSION['gebruiker'] = array('id' => $arr['id'], 'username' => $arr['voornaam']);
    } 
}
if (is_array($_SESSION['gebruiker'])) {
    
    // eind voorkomen van SQL Injection
    
    header ('Location: ../admin/eventz.php');

}
 ?>
<!DOCTYPE html>
<html lang="nl">
<title>Login</title>
    <head>
        <!-- Meta's -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="" />

         <title>Sander ten Thije 2015</title>
        
        <!-- Css Connection -->
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">

        <!-- Source Connection -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    </head>
    <body>

        <!-- Header -->
        <nav>
         
        </nav>
        
        <!-- Main content -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <?php 
                        if (isset($_POST['gebruikersnaam']) and isset($_POST['wachtwoord'])) {
                            echo $error; 
                        }
                    ?>
                    
                        <div class="panel-heading">
                            <h3 class="panel-title">inloggen</h3>
                        </div>
                        <div class="panel-body">
                            <form accept-charset="UTF-8" role="form" action="" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Gebruikersnaam" name="gebruikersnaam" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Wachtwoord" name="wachtwoord" type="password" >
                                </div>
                                <a href="../index.php" > Terug naar de homepage</a>
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                            </fieldset>
                            </form>
                            
                        </div>
                    </div>
                </div>
            
        </div>
        
        <!-- Footer Wrap -->
        <div class="footer-wrap">
        
        </div>
        

        <!-- JavaScript Connection -->
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/npm.js"></script>
    </body>
</html>
