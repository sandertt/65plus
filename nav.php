<?php
$link = mysqli_connect("localhost","root","","database") or die("Error " . mysqli_error($link));
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="index.php"><img src="img/logo1.png" /> </a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="overzicht.php">events</a>
                </li>
                <li>
                    <a class="page-scroll" href="about.php">Over ons</a>
                </li>
                <li>
                    <a class="page-scroll" href="organisaties.php">organisatie</a>
                </li>
<?php if(!isset($_SESSION['gebruiker'])) {
?>
                <li>
                    <a class="page-scroll" href="signup.php">Aanmelden als organistor</a>
                </li>
<?php
}
?>
                <li>
                    <?php
                    if(isset($_SESSION['gebruiker'])) {
                       $query = "SELECT * FROM `gebruikers` WHERE `id` = '".$_SESSION['gebruiker']['id']."'";
                       $result = $link->query($query);
                       while($row = $result->fetch_array()) {

                            ?>
                            <a class="page-scroll" href="login/login.php">Terug naar je
                                dashboard, <?php echo $row ['naam']; ?></a>
                            <?php
                        }

}


else {

echo   ' <a class="page-scroll" href="login/login.php">Inloggen</a> ' ;

}
?>

                </li>


        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
