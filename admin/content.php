<?php
session_start();
include 'functions.php';
if (!isset($_SESSION['gebruiker'])) {
    header("location: ../login/login.php");
    exit();
}

$done = '<div class="alert alert-success">
  <strong>Gelukt!</strong> Je veranderingen zijn opgeslagen.
</div>'



?>
<?php
error_reporting(0);
$link = mysqli_connect("localhost", "root", "", "database") or die("Error " . mysqli_error($link));
$query = "SELECT * FROM `gebruikers` WHERE `id` = '" . $_SESSION['gebruiker']['id'] . "'";
$result = $link->query($query);

while ($row = $result->fetch_array()) {
    $vestigingID = $row['locatie'];
}



if (isset($_POST['prijzensubmit'])) {
    $updatequery = 'UPDATE `markup` SET  `content` = "' . $_POST['content'] . '", `naam` = "' . $_POST['naam'] . '", `h1` = "' . $_POST['h1'] . '", `p` = "' . $_POST['p'] . '", `psize` = "' . $_POST['psize'] . '", `open` = "' . $_POST['open'] . '", `telefoon` = "' . $_POST['telefoon'] . '", `afbeelding` = "' . $_POST['afbeelding'] . '", `about` = "' . $_POST['about'] . '", `aboutimg` = "' . $_POST['aboutimg'] . '" WHERE `id` = "' . $vestigingID . '"';

    $link->query($updatequery);
}


$currentPrices = $link->query("SELECT * FROM `markup` WHERE `id` = '" . $vestigingID . "'");
while ($row = $currentPrices->fetch_array()) {
    $currPrices['lpg'] = $row['LPG'];
    $currPrices['euro95'] = $row['euro95'];
    $currPrices['diesel'] = $row['diesel'];
    $currPrices['h1'] = $row['h1'];
    $currPrices['p'] = $row['p'];
    $currPrices['psize'] = $row['psize'];
    $currPrices['content'] = $row['content'];
    $currPrices['telefoon'] = $row['telefoon'];
    $currPrices['open'] = $row['open'];
    $currPrices['naam'] = $row['naam'];
    $currPrices['afbeelding'] = $row['afbeelding'];
    $currPrices['about'] = $row['about'];
    $currPrices['aboutimg'] = $row['aboutimg'];
}

$link->close();


?>

<html>
<head>
    <link href="main.css" rel="stylesheet" style="text/css"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Merriweather Sans">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Oxygen">
    <link rel="stylesheet" type="text/css" href="buttons.css">
    <link rel="stylesheet" type="text/css" href="navcss/bootstrap.css">


</head>
<title>Content-Admin</title>
<body>


<div class="row">
    <div class="header">Content</div>
    <?php include "demo.php"; ?>
    <div class="container">
        <?php
        checkadmin2();
        if($_SESSION['admin'] >= 2) { ?>
        <?php
        if (isset($_POST['prijzensubmit'])) {
            echo $done;
        }
        ?>
        <form name="prijzen" method="post" action="">

            <div style="top: -15px;" class="large-6 columns">
                <div class="form-group">
                    <h2 for="name">Naam locatie </h2> <input type="text" class="form-control" id="name"
                                                                   name="naam" placeholder="Naam van vestiging"
                                                                   value="<?php echo $currPrices['naam']; ?>"
                                                                   class="input"></div>
                </br>
                <div class="form-group">
                    <h2 for="name">Telefoon </h2><input type="text" class="form-control" id="name"
                                                              name="telefoon"
                                                              placeholder="Telefoonnummer"
                                                              value="<?php echo $currPrices['telefoon']; ?>"
                                                              class="input"></div>
                </br>
                <div class="form-group">
                    <h2 for="name">Openingstijden </h2><input type="text" class="form-control" id="name"
                                                                    name="open" placeholder="vb: 10:00-02:00"
                                                                    value="<?php echo $currPrices['open']; ?>"
                                                                    class="input"></div>
            </div>
            <div class="large-12 columns">
                <h3>Tekst grootte</h3>
                <input type="number" name="psize" placeholder="15" value="<?php echo $currPrices['psize']; ?>"
                       class="input">

                <h3>Header kleur</h3> <input type="text" class="jscolor" name="h1"
                                             placeholder="vb: red / blue / grey"
                                             value="<?php echo $currPrices['h1']; ?>"
                                             class="input"></br>
                <h3>Tekst kleur</h3> <input type="text" class="jscolor" name="p"
                                            placeholder="vb: red / blue / grey"
                                            value="<?php echo $currPrices['p']; ?>"
                                            class="input"></br>


                <div class="form-group">
                    <h2 for="name">CONTENT </h2> <textarea rows="5" type="text" class="form-control" id="name"
                                                                 name="content" placeholder="Content" value=""
                                                                 class=""><?php echo $currPrices['content']; ?></textarea>
                </div>
                <br>

                <h2>Afbeelding home page</h2> <input type="text" name="afbeelding"
                                           placeholder="URL naar afbeelding"
                                           value="<?php echo $currPrices['afbeelding']; ?>"
                                           class="input"></br></br>
<hr>
                <h2>Afbeelding about pagina</h2> <input type="text" name="aboutimg"
                                                     placeholder="URL naar afbeelding"
                                                     value="<?php echo $currPrices['aboutimg']; ?>"
                                                     class="input"></br></br>
                <h2>About pagina inhoud text</h2> <textarea type="text" name="about" class="form-control"
                                                     placeholder="URL naar afbeelding"
                                                     value=""><?php echo $currPrices['about']; ?>
                                                     </textarea></br></br>


                <input type="submit" name="prijzensubmit" value="Aanpassen" class="ok"/>


            </div>
        </form>
    </div>


</div>

<script src="jscolor.js"></script>
<script src="jscolor.min.js"></script>
<!-- Custom Theme JavaScript -->

<script>
    function setTextColor(picker) {
        document.getElementsByTagName('body')[0].style.color = '#' + picker.toString()
    }
</script>
<?php
}
else  {
    echo 'Geen rechten voor deze pagina';
}
?>

</body>
</html>

