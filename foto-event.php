<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">


<div class="w3-content w3-display-container">
    <?php
    if (!empty($row['img2'])) {
        ?>
        <a class="w3-btn-floating w3-hover-dark-grey w3-display-left" onclick="plusDivs(-1)">&#10094;</a>
        <a class="w3-btn-floating w3-hover-dark-grey w3-display-right" onclick="plusDivs(1)">&#10095;</a>
    <?php
    }
    ?>
    <div class="w3-display-container mySlides">
        <img src="<?php echo $row['img']; ?> " style="height:100%; width: 100%;">
        <div class="w3-display-bottomleft w3-large w3-container w3-padding-16 w3-black">
            <?php echo $row['imgbeschrijving1']; ?>
        </div>
    </div>
    <?php
    if (!empty($row['img2'])) {
        ?>
        <div class="w3-display-container mySlides">
            <img src="<?php echo $row['img2']; ?>" style="height:100%; width: 100%;">
            <div class="w3-display-bottomright w3-large w3-container w3-padding-16 w3-black">
                <?php echo $row['imgbeschrijving2']; ?>
            </div>
        </div>
    <?php
    }
    ?>
    <?php
    if (!empty($row['img3'])) {
    ?>
    <div class="w3-display-container mySlides">
        <img src="<?php echo $row['img3']; ?>" style="height:100%; width: 100%;">
        <div class="w3-display-topleft w3-large w3-container w3-padding-16 w3-black">
            <?php echo $row['imgbeschrijving3']; ?>
        </div>
    </div>
    <?php
    }
    ?>
</div>

<script>
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length}
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex-1].style.display = "block";
    }
</script>
