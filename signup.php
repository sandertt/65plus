<?php
session_start();
include 'functions.php';

$done = '<div class="alert alert-ok alert-dismissible" role="alert">
                  <button align="center" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <strong>Succes!</strong> U kan vanaf nu aanmelden
                  </div>';

?>
<?php
$link = mysqli_connect("localhost","root","","database");
if(isset ($_POST['gebruikersubmit']))
{
    echo $done;
    $wachtwoordmd5 = $_POST['wachtwoord'];
    $wachtwoord = md5($wachtwoordmd5);
    $query  = 'INSERT INTO `gebruikers` (gebruikersnaam,wachtwoord,naam,organisatie,functie) VALUES ("'.$_POST['gebruikersnaam'].'","'.$wachtwoord.'","'.$_POST['naam'].'","'.$_POST['organisatie'].'","'.$_POST['functie'].'")';
    $link -> query($query);
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include 'head.php' ?>
<body id="page-top" class="index">

<!-- Navigation -->


<?php include "nav.php"; ?>
<div class = "container">

    <form name = "gebruikers" method = "post" action = "signup.php">
        <div class = "header"><h1>Aanmelden als organistor</h1></div>
        <h2>Gebruikersnaam</h2><input type = "text" name = "gebruikersnaam" placeholder = "gebruikersnaam" class = "form-control" required ></br>
        <h2>Wachtwoord</h2> <input type = "password" name = "wachtwoord" placeholder = "Wachtwoord" class = "form-control" required ></br>
        <h2>Uw hele naam</h2> <input type = "text" name = "naam" placeholder = "Voornaam" class = "form-control" required ></br>
        <h2>Welke organisatie bent u?</h2> <input type = "text" name = "organisatie" placeholder = "Achternaam" class = "form-control" required ></br>
        <h2>Ik ga akkoord</h2>
        <input type="radio" name="functie" value="1" required title="Uw moet akkoord gaan met de voorwaarden"> Ja<br>
        <p style="font-style:italic; font-weight: bolder;" class="voorwaarden-p"> Bekijk onze voorwaarden:</p>
        <p class="voorwaarden"> Dit zijn al onze voorwaarden. Wij streven naar een rechtvaardig gebruik van de website. u bent niet toegestaan te spammen
        of ideeën te kopiëren van andere gebruikers. Voor verdere vragen kan u ons bellen op: 071-5388761. Wij houden de site en uw gebruik altijd
        in de gaten.</p>
        <br/>
        <br/>
        <input type = "submit" name = "gebruikersubmit" value = "aanmelden" class = "btn btn-lg btn-success btn-block"/>
        <?php
        $done = '<div class="alert alert-ok alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <strong>Succes!</strong> U kan vanaf nu aanmelden
                  </div>';


        ?>
    </form>
</div>
<?php include 'scripts.php' ?>
<style>
    form {
        margin-top: 125px;
    }
  .alert-ok {
      background-color: green !important;
      top: 95px;
      width: 50%;
      position: relative;
      margin: 0 auto;
      display: block;
      margin-bottom: 91px;
  }
</style>




